#!/usr/bin/env python
# Anonymize CSV files
# Currently this is all in one file for both anonymization, and de-anonymization
# may well be split into others later
#
#
# Possible future enhancements:
# - When anonymizing, store the mapping of anonymized->plain values in a mapping file
#   which can later be used to return anonymized values in CSV file to the plain/original
# - de-anonymizing using said map
# - cloud-nativeness e.g. scalability with sqs/lambda
#       scalability, distributing anonymization workload with fanout/queue-based (for large datasets)
# - storing mappings in a database e.g. dynamodb
# - web-based frontend?
# - [un]zip, streaming (storage and memory optimization, respectively)
# - automatically prohibit/punish editing with emacs j/k

import argparse
import unicodecsv as csv
import sys
from faker import Faker
from collections import defaultdict
from pprint import pprint

fake = Faker('en_AU')
parser = argparse.ArgumentParser(description='Anonymize CSV files. If --map is supplied, use that to convert anonymized->plain. Otherwise')

parser.add_argument('--plain', help='Filename of plain/original data')
parser.add_argument('--anonymized', help='Filename of anonymized data')
parser.add_argument('--map', help='Filename for mapping of anonymized->original values')
parser.add_argument('-test', '-t', help='Only test, don\'t write to files - print the first 5 records that would be output')
#parser.add_argument('-y', action='store_true', help='Assume yes on all confirmations, uncluding file overwrite (use when running non-interactively)')
args = parser.parse_args()

# TODO implement -y to say yes to:
# - confirmation whether we're anonymizing or un-anonymizing (based on --map supplied)
# - confirm it's ok to overwrite an existing output file
# (this would typically be used in automated / non-interactive running)

# TODO check that if we're not testing, both input and output files are supplied
# This is to permit -t to see what would happen, without necessarily specifying an output file

# TODO if output file is supplied and it exists - either bail entirely (raise Exception('oh no'))
# or ask for confirmation


# TODO probably generalize & ensmarten this based on headers and/or field values:
# "if a field looks like an email then anonymize it as such"
# But for now it's static
fakes = defaultdict(lambda: {
    'User Name': fake.email(),
    'First Name': fake.first_name(),
    'Last Name': fake.last_name()
})

# If we're anonymizing
if(args.plain and args.anonymized):

    # TODO check output/anonymized file exists and confirm/die (your choice)
    with open(args.anonymized) as anonymized:
        with open(args.plain, 'r') as plain:
            reader = csv.DictReader(plain)
            for row in reader:
                # If fakes[User Name] doesn't exist (i.e. fakes is keyed on username)
                # then the defaultdict lambda function will create & store one,
                # with fake values for email, first_name etc
                fakerow = fakes[row['User Name']]

                # replace values in the input row with the faked ones
                # (if they have the same field names, will overwrite)
                row.update(fakerow)

                # write the faked row
                print(row)
                print

# We're de-anonymizing ... open the mapping file, for each row replace with looked-up values etc
elif (args.map and args.plain and args.anonymized):
    # TODO if we're de-anonymizing
    raise Exception('de-anonymizing not yet implemented')

# TODO User didn't supply -t, or (plain and anon) ... explain usage to them
else:
    sys.exit("""Either
        1. To test (-t) you supply (some other valid combo below)
        2. To anonymize WITHOUT A CHANCE TO DE-ANONYMIZE, you're supplying --plain and --anonymized, OR
        3. To anonymize and store a mapping (ability to de-anon), supply --plain --anonymized --mapping, OR
        4. To de-anonymize, supply --plain, --anonymized, --mapping (where only the latter 2 files exist)""")
